MOTIVASI:
Keberhasilan bukan tentang seberapa sering Anda jatuh, tetapi seberapa sering Anda bangkit kembali.