<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>String PHP</title>
</head>

<body>
    <h1>Berlatih String PHP</h1>

    <?php
    echo "<h3> Soal No 1</h3>";
    /* SOAL NO 1: Tunjukkan dengan menggunakan echo berapa panjang dari string yang diberikan berikut!
    Tunjukkan juga jumlah kata di dalam kalimat tersebut!
    Contoh: $string = "PHP is never old"; Output: Panjang string: 16, Jumlah kata: 4
    */
    $first_sentence = "Hello PHP!";
    $second_sentence = "I'm ready for the challenges";

    // Menghitung panjang string menggunakan strlen
    $panjang_string1 = strlen($first_sentence);
    $panjang_string2 = strlen($second_sentence);

    // Menghitung jumlah kata menggunakan str_word_count
    $jumlah_kata1 = str_word_count($first_sentence);
    $jumlah_kata2 = str_word_count($second_sentence);

    // Menampilkan hasil
    echo "String 1: \"$first_sentence\"<br>";
    echo "Panjang string: $panjang_string1, Jumlah kata: $jumlah_kata1<br>";

    echo "String 2: \"$second_sentence\"<br>";
    echo "Panjang string: $panjang_string2, Jumlah kata: $jumlah_kata2<br>";

    echo "<h3> Soal No 2</h3>";
    /* SOAL NO 2: Mengambil kata pada string dan karakter-karakter yang ada di dalamnya. */
    $string2 = "I love PHP";

    echo "<label>String: </label> \"$string2\"<br>";
    echo "Kata pertama: " . substr($string2, 0, 1) . "<br>";
    echo "Kata kedua: " . substr($string2, 2, 4) . "<br>";
    echo "Kata ketiga: " . substr($string2, 7, 3) . "<br>";

    echo "<h3> Soal No 3 </h3>";
    /* SOAL NO 3: Mengubah karakter atau kata yang ada di dalam sebuah string. */
    $string3 = "PHP is old but sexy!";
    echo "String: \"$string3\"<br>";

    $string3 = str_replace("old", "awesome", $string3);

    echo "String setelah diubah: \"$string3\"";
    ?>
</body>

</html>