<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Function</title>
</head>

<body>
    <h1>Berlatih Function PHP</h1>

    <?php
    echo "<h3> Soal No 1 Greetings </h3>";

    function greetings($nama)
    {
        echo "Halo " . ucfirst($nama) . ", Selamat Datang di Sanbercode!";
    }

    greetings("Bagas");
    echo "<br>";
    greetings("Wahyu");
    echo "<br>";
    greetings("nama peserta");
    echo "<br>";

    echo "<br><br>";
    echo "<h3>Soal No 2 Reverse String</h3>";

    function reverseString($str)
    {
        $length = strlen($str);
        $reversed = "";
        for ($i = $length - 1; $i >= 0; $i--) {
            $reversed .= $str[$i];
        }
        echo $reversed;
    }

    reverseString("Krisna Jorgi");
    echo "<br>";
    reverseString("Sanbercode");
    echo "<br>";
    reverseString("We Are Sanbers Developers");
    echo "<br>";

    echo "<br>";
    echo "<h3>Soal No 3 Palindrome </h3>";

    function palindrome($str)
    {
        $str = strtolower(str_replace(' ', '', $str));
        $length = strlen($str);
        for ($i = 0; $i < $length / 2; $i++) {
            if ($str[$i] != $str[$length - 1 - $i]) {
                echo "false";
                return;
            }
        }
        echo "true";
    }

    palindrome("civic");
    echo "<br>"; // true
    palindrome("nababan");
    echo "<br>"; // true
    palindrome("jambaban");
    echo "<br>"; // false
    palindrome("racecar");
    echo "<br>"; // true

    echo "<h3>Soal No 4 Tentukan Nilai </h3>";

    function tentukan_nilai($nilai)
    {
        if ($nilai >= 85 && $nilai <= 100) {
            echo "Sangat Baik";
        } elseif ($nilai >= 70 && $nilai < 85) {
            echo "Baik";
        } elseif ($nilai >= 60 && $nilai < 70) {
            echo "Cukup";
        } else {
            echo "Kurang";
        }
    }

    tentukan_nilai(98);
    echo "<br>"; // Sangat Baik
    tentukan_nilai(76);
    echo "<br>"; // Baik
    tentukan_nilai(67);
    echo "<br>"; // Cukup
    tentukan_nilai(43);
    echo "<br>"; // Kurang
    ?>
</body>

</html>