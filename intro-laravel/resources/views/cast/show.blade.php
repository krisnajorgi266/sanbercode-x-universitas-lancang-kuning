@extends('layouts.master')

@section('title')
    Detail
@endsection

@section('content')
    <table class="table">   
  <thead>
    <tr>
      <th scope="col">No</th>
      <th scope="col">Nama</th>
      <th scope="col">Umur</th>
      <th scope="col">Bio</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <th scope="row">{{ $cast->id}}</th>
      <td>{{ $cast->nama}}</td>
      <td>{{ $cast->umur}} </td>
      <td>{{ $cast->bio }}</td>
    </tr>
  </tbody>
  <a href="/cast" class="btn btn-primary mb-3">Kembali</a>
</table>
@endsection