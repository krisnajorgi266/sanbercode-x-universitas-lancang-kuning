@extends('layouts.master')

@section('title')
    Buat Account Baru!    
@endsection

@section('content')
    <h3> Sign Up Form</h3>
    <form action="/welcome" method="POST">
        @csrf
        <label for="">First Name : </label><br>
        <input type="text" name="depan" placeholder="Masukkan Nama Depan" required><br><br>

        <label for="">Last Name : </label><br>
        <input type="text" name="belakang" placeholder="Masukkan Nama Belakang" required><br><br>

        <label>Gender:</label><br>
        <input type="radio" name="gender" value="male">Male <br>
        <input type="radio" name="gender" value="female">Female <br><br>
        
        <label> Nationaly:</label><br>
            <select name="nationaly"><br><br>
                <option value="indonsia">Indonesia</option>
                <option value="malaysia">Malaysia</option>
                <option value="brunei">Brunei</option>
                <option value="myanmar">Myanmar</option>
            </select><br><br>
        </select>

        <label>Language Spoken:</label><br>
        <input type="checkbox" name="language">Indonesia <br>
        <input type="checkbox" name="language">English <br>
        <input type="checkbox" name="language">Japan <br>
        <p>Bio:</p>
        <textarea name="bio" rows="10" cols="30"></textarea>
        <br><br>
        <button type="submit"value="Sign Up">Register</button>
    </form>
@endsection
    