<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class AuthController extends Controller
{
    public function form() {
        return view ('pages.register');
    }

    public function store(Request $request ) {
        $nama_depan = $request ['depan'];
        $nama_belakang = $request ['belakang'];

        // return view ('pages.welcome'  , ['depan'=> $nama_depan, 'belakang' => $nama_belakang]);
        return view('pages.welcome', compact('nama_depan', 'nama_belakang'));
    }
}
