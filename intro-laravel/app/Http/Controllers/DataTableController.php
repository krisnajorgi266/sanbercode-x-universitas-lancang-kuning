<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class DataTableController extends Controller
{
    public function tampilDatatable()
    {
        return view ('tables.data-table');
    }
}
